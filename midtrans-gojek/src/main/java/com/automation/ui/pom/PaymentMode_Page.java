package com.automation.ui.pom;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

import com.automation.ui.utilities.BaseTest;

/**
 * @author Ankush
 * @category Page
 * 
 */
public class PaymentMode_Page {

	WebDriver driver;
	Logger logger = LogManager.getLogger(PaymentMode_Page.class);

	@FindBy(how = How.XPATH, using = "//div[@class='list-title text-actionable-bold' and text()='Credit Card']")
	WebElement creditCard_option;

	@FindBy(how = How.XPATH, using = "//p[@class='text-page-title-content']")
	WebElement payment_span;

	public PaymentMode_Page(WebDriver driver) {
		this.driver = driver;

	}
	
	public void click_creditCard_option() {

		BaseTest.explicitWait(creditCard_option);
		creditCard_option.click();
		logger.info("Clicked on Credit Card Option");

	}

	public String getText_payment_span() {
		return payment_span.getText();
	}
}
