package com.automation.ui.pom;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import com.automation.ui.utilities.BaseTest;

/**
 * @author Ankush
 * @category Page
 * 
 */

public class OrderSummary_Page {

	WebDriver driver;
	Logger logger = LogManager.getLogger(OrderSummary_Page.class);

	@FindBy(how = How.XPATH, using = "//span[text()='order details']")
	WebElement orderDetails_tab;

	@FindBy(how = How.XPATH, using = "//span[text()='shipping details']")
	WebElement shoppingDetails_tab;

	@FindBy(how = How.XPATH, using = "//td[@class='table-amount text-body']")
	WebElement amount_textBody;

	@FindBy(how = How.XPATH, using = "(//div[@class='text-body'])[1]")
	WebElement name_textBody;

	@FindBy(how = How.XPATH, using = "(//div[@class='text-body'])[2]")
	WebElement phoneNo_textBody;

	@FindBy(how = How.XPATH, using = "(//div[@class='text-body'])[3]")
	WebElement email_textBody;

	@FindBy(how = How.XPATH, using = "(//div[@class='text-body'])[4]")
	WebElement address_textbody;

	@FindBy(how = How.XPATH, using = "//a[@class='button-main-content']")
	WebElement continue_Button;

	public OrderSummary_Page(WebDriver driver) {
		this.driver = driver;

	}

	public void click_orderDetails_tab() {

		BaseTest.explicitWait(orderDetails_tab);
		orderDetails_tab.click();
		logger.info("Clicked on Order Details Tab");

	}

	public String getText_amount_textBody() {

		BaseTest.explicitWait(amount_textBody);
		String text = amount_textBody.getText();
		logger.info("Amount captured succeefully : "+text);
		return text;

	}

	public void click_shoppingDetails_tab() {

		BaseTest.explicitWait(shoppingDetails_tab);
		shoppingDetails_tab.click();
		logger.info("Clicked on Shipping Details Tab");

	}

	public String getText_name_textBody() {

		BaseTest.explicitWait(name_textBody);
		String text = name_textBody.getText();
		logger.info("Name captured succeefully : "+text);
		return text;

	}

	public String getText_phoneNo_textBody() {

		BaseTest.explicitWait(phoneNo_textBody);
		String text = phoneNo_textBody.getText();
		logger.info("phone Number captured succeefully : "+text);
		return text;

	}
	
	
	
	public String getText_email_textBody() {

		BaseTest.explicitWait(email_textBody);
		String text = email_textBody.getText();
		logger.info("Email captured succeefully : "+text);
		return text;

	}
	
	public String getText_address_textbody() {

		BaseTest.explicitWait(address_textbody);
		String text = address_textbody.getText();
		logger.info("Address captured succeefully : "+text);
		return text;

	}
	
	public void click_continue_Button() {

		BaseTest.explicitWait(continue_Button);
		continue_Button.click();
		logger.info("Clicked on Continue Button");

	}

	

}
