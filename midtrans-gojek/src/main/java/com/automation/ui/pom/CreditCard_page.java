package com.automation.ui.pom;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

import com.automation.ui.utilities.BaseTest;

/**
 * @author Ankush
 * @category Page
 * 
 */

public class CreditCard_page {
	WebDriver driver;
	Logger logger = LogManager.getLogger(PaymentMode_Page.class);

	@FindBy(how = How.NAME, using = "cardnumber")
	WebElement creditCardNumber_TextBox;

	@FindBy(how = How.XPATH, using = "//input[@placeholder='MM / YY']")
	WebElement expiryData_TextBox;

	@FindBy(how = How.XPATH, using = "//input[@inputmode='numeric']")
	WebElement CVV_TextBox;

	@FindBy(how = How.XPATH, using = "//a[@class='button-main-content']")
	WebElement PayNow_button;
	
	@FindBy(how = How.XPATH, using = "//h1[text()='Issuing Bank']")
	WebElement IssuingBank_Label;
	
	
	@FindBy(how = How.ID, using = "PaRes")
	WebElement password_Textbox;
	
	@FindBy(how = How.NAME, using = "ok")
	WebElement ok_button;
	
	@FindBy(how = How.XPATH, using = "//span[text()='Thank you for your purchase.']")
	WebElement thanksHome_Label;
	
	@FindBy(how = How.XPATH, using = "//span[text()='Invalid card number']")
	WebElement invalidCardDetails_Msg;
	
	
	public CreditCard_page(WebDriver driver) {
		this.driver = driver;

	}
	
	public void enter_creditCardNumber_TextBox(String number) {

		BaseTest.explicitWait(creditCardNumber_TextBox);
		creditCardNumber_TextBox.sendKeys(number);
		logger.info("Credit Card Number "+number+" Successfully entered");

	}

	public void enter_expiryData_TextBox(String expiry) {

		BaseTest.explicitWait(expiryData_TextBox);
		expiryData_TextBox.sendKeys(expiry);
		logger.info("Credit Card Expiry date "+expiry+" Successfully entered");

	}
	
	public void enter_CVV_TextBox(String CVV) {

		BaseTest.explicitWait(CVV_TextBox);
		CVV_TextBox.sendKeys(CVV);
		logger.info("Credit Card Expiry date "+CVV+" Successfully entered");

	}
	
	
	public void click_PayNow_button() {

		BaseTest.explicitWait(PayNow_button);
		PayNow_button.click();
		logger.info("Clicked on PayNow Button");

	}
	
	public String get_IssuingBank_Label() {

		BaseTest.explicitWait(IssuingBank_Label);
		String text=IssuingBank_Label.getText();
		logger.info("Captured IssueBank Label");
		return  text;

	}
	
	public void enter_password_Textbox(String password) {

		BaseTest.explicitWait(password_Textbox);
		password_Textbox.sendKeys(password);
		logger.info("Credit Card password "+password+" successfully entered");

	}

	public void click_ok_button() {

		BaseTest.explicitWait(ok_button);
		ok_button.click();
		logger.info("Clicked on Ok Button");

	}
	
	public String get_thanksHome_Label() {

		BaseTest.explicitWait(thanksHome_Label);
		String text=thanksHome_Label.getText();
		logger.info("Captured thanks Label");
		return  text;

	}
	
	public String get_InvalidCardDetailsMsg() {

		BaseTest.explicitWait(invalidCardDetails_Msg);
		String text=invalidCardDetails_Msg.getText();
		logger.info("Captured invalidCardDetails message");
		return  text;

	}
	
	

}
