package com.automation.ui.pom;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import com.automation.ui.utilities.BaseTest;


/**
 * @author Ankush
 * @category Page
 * 
 */

public class ShoppingCart_Page {

	WebDriver driver;
	Logger logger=LogManager.getLogger(ShoppingCart_Page.class);

	@FindBy(how = How.XPATH, using = "//a[contains(text(),'BUY NOW')]")
	WebElement buyNow_button;

	@FindBy(how = How.XPATH, using = "//span[contains(text(),'Shopping Cart')]")
	WebElement shoppingCart_span;

	@FindBy(how = How.XPATH, using = "//input[@value='Budi']")
	WebElement name_textBox;

	@FindBy(how = How.XPATH, using = "//input[@type='email']")
	WebElement email_textBox;

	@FindBy(how = How.XPATH, using = "//td[text()='Phone no']//following::input[1]")
	WebElement phoneNo_textBox;

	@FindBy(how = How.XPATH, using = "//td[text()='Phone no']//following::input[2]")
	WebElement city_textBox;

	@FindBy(how = How.XPATH, using = "//textarea")
	WebElement address_textarea;

	@FindBy(how = How.XPATH, using = "//td[text()='Postal Code']//following::input[1]")
	WebElement postalCode_textBox;
	
	@FindBy(how = How.XPATH, using = "//div[@class='cart-checkout']")
	WebElement checkOut_button;

	public ShoppingCart_Page(WebDriver driver) {
		this.driver = driver;

	}

	public void click_BuyButton() {
		BaseTest.explicitWait(buyNow_button);
		buyNow_button.click();
		logger.info("Clicked on Buy Now Button");
		BaseTest.explicitWait(shoppingCart_span);

	}

	public void enter_name(String name) {

		BaseTest.explicitWait(name_textBox);
		name_textBox.clear();
		logger.info("Cleared name TextBox");
		name_textBox.sendKeys(name);
		logger.info("Entered "+name+" in Name TextBox");
	}

	public void enter_email(String email) {

		BaseTest.explicitWait(email_textBox);
		email_textBox.clear();
		logger.info("Cleared Email TextBox");
		email_textBox.sendKeys(email);
		logger.info("Entered "+email+" in Email TextBox");
	}

	public void enter_phoneNo(String phoneNo) {

		BaseTest.explicitWait(phoneNo_textBox);
		phoneNo_textBox.clear();
		logger.info("Cleared PhoneNo TextBox");
		phoneNo_textBox.sendKeys(phoneNo);
		logger.info("Entered "+phoneNo+" in PhoneNo TextBox");
	}
	
	public void enter_city(String city) {

		BaseTest.explicitWait(city_textBox);
		city_textBox.clear();
		logger.info("Cleared city TextBox");
		city_textBox.sendKeys(city);
		logger.info("Entered "+city+" in City TextBox");
	}
	
	public void enter_address(String address) {

		BaseTest.explicitWait(address_textarea);
		address_textarea.clear();
		logger.info("Cleared address Textarea");
		address_textarea.sendKeys(address);
		logger.info("Entered "+address+" in Address Textarea");
	}
	
	public void enter_postal(String postalCode) {

		BaseTest.explicitWait(postalCode_textBox);
		postalCode_textBox.clear();
		logger.info("Cleared postal Code TextBox");
		postalCode_textBox.sendKeys(postalCode);
		logger.info("Entered "+postalCode+" in Postal Code TextBox");
	}
	

	public void click_checkOutButton() {
		BaseTest.explicitWait(checkOut_button);
		checkOut_button.click();
		logger.info("Clicked on CheckOut button");
		

	}
	
}
