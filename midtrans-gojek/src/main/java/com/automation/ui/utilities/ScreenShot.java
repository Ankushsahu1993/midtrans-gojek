package com.automation.ui.utilities;

import java.io.File;
import java.io.IOException;
import java.util.Random;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
/**
 * @author Ankush
 * @category utility
 */
public class ScreenShot {
	
	private static String path=System.getProperty("user.dir");
	
public static void getScreenShot(WebDriver driver,String name) throws IOException{
		
		TakesScreenshot ts=(TakesScreenshot)driver;
		File source=ts.getScreenshotAs(OutputType.FILE);
		Random rand= new Random();
		int randomNumber=rand.nextInt(10);
		FileUtils.copyFile(source, new File(path+"\\ScreenShots\\"+name+""+randomNumber+".png"));
		System.out.println(path+"aa.png");
	}

}
