package com.automation.ui.utilities;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

public class ReadExcelData {
	
	public static FileInputStream fis;
	public static HSSFWorkbook workbook;
	public static HSSFSheet sheet;
	public static Object[][] arr=null;
	private static String path=System.getProperty("user.dir");
	public static Object[][] readUserDetails() throws IOException {
		
		
		int cellCount;
		int rowCount;
		fis= new FileInputStream(new File(path+"\\testData\\credentials.xls"));
		workbook= new HSSFWorkbook(fis);
		sheet=workbook.getSheet("UserDetails");	
		
				
				rowCount=sheet.getLastRowNum();
				cellCount=sheet.getRow(0).getLastCellNum();
				arr= new Object[rowCount][cellCount];
				for(int i=0;i<rowCount;i++) {
					
					for(int j=0;j<cellCount;j++) {
						
						//System.out.println(sheet.getRow(i).getCell(j).getStringCellValue());
						arr[i][j]=sheet.getRow(i+1).getCell(j).getStringCellValue();
						
					}
					
				}
				return arr;			
				
				
				
			}

	
public static Object[][] readCardDetails() throws IOException {
		
		
		int cellCount;
		int rowCount;
		fis= new FileInputStream(new File(path+"\\testData\\credentials.xls"));
		workbook= new HSSFWorkbook(fis);
		sheet=workbook.getSheet("CardDetails");	
		
				
				rowCount=sheet.getLastRowNum();
				cellCount=sheet.getRow(0).getLastCellNum();
				arr= new Object[rowCount][cellCount];
				for(int i=0;i<rowCount;i++) {
					
					for(int j=0;j<cellCount;j++) {
						
						//System.out.println(sheet.getRow(i).getCell(j).getStringCellValue());
						arr[i][j]=sheet.getRow(i+1).getCell(j).getStringCellValue();
						
					}
					
				}
				return arr;			
				
				
				
			}

	
		
public static Object[][] readInvalidCardDetails() throws IOException {
	
	
	int cellCount;
	int rowCount;
	fis= new FileInputStream(new File(path+"\\testData\\credentials.xls"));
	workbook= new HSSFWorkbook(fis);
	sheet=workbook.getSheet("InvalidCardDetails");	
	
			
			rowCount=sheet.getLastRowNum();
			cellCount=sheet.getRow(0).getLastCellNum();
			arr= new Object[rowCount][cellCount];
			for(int i=0;i<rowCount;i++) {
				
				for(int j=0;j<cellCount;j++) {
					
					//System.out.println(sheet.getRow(i).getCell(j).getStringCellValue());
					arr[i][j]=sheet.getRow(i+1).getCell(j).getStringCellValue();
					
				}
				
			}
			return arr;			
			
			
			
		}


		
	
	

}
