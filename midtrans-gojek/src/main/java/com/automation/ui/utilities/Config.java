package com.automation.ui.utilities;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;
/**
 * @author Ankush
 * @category utility
 * 
 */

//This Config class will read the data from config.properties file
//
public class Config {

	private static String path=System.getProperty("user.dir");
	
	public static String getData(String key) throws IOException {
		
		
		FileInputStream fis= new FileInputStream(new File(path+"\\config.properties"));
		
		Properties prop= new Properties();
		
		prop.load(fis);
		
		String env=prop.getProperty(key);
		
		System.out.println(env);
		
		return env;
		
		
		
		
		
	}
	
	
	
}
