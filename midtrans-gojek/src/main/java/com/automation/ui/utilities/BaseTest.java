package com.automation.ui.utilities;
import java.util.concurrent.TimeUnit;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 * @author Ankush
 * @category utility
 * 
 */
public class BaseTest {
	
	public static WebDriver driver;
	public static WebDriverWait wait;
	public static String path=System.getProperty("user.dir");
	public static Logger logger=LogManager.getLogger(BaseTest.class);
	
	
	public static void explicitWait(WebElement element) {
		
		 wait= new WebDriverWait(driver, 30);
		 wait.until(ExpectedConditions.visibilityOf(element));
	}
	
	
	
	
	public static void navigate(String url) {
	
		driver.navigate().to(url);
		logger.info("Navigated to "+url);
		
	}
	
	public static void switchToFrame() {
		driver.switchTo().frame(0);
		logger.info("Switched to frame");
		
	}

	public static void switchToParentFrame() {
		driver.switchTo().parentFrame();
		logger.info("Switched to Parent frame");
		
	}
	
	public static void switchToDefaultContent() {
		driver.switchTo().defaultContent();
		logger.info("Switched to default Default");
		
	}

	
	
	public static void setup() {
		
		driver.manage().window().maximize();
		logger.info("Browser maximized");
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		driver.manage().timeouts().pageLoadTimeout(120, TimeUnit.SECONDS);
		driver.manage().deleteAllCookies();
	}
	
	
	public static  void browser(String browserName) {
		
		
		if(browserName.equalsIgnoreCase("chrome")) {
			
			System.setProperty("webdriver.chrome.driver", path+"\\Drivers\\chromedriver.exe");
			driver= new ChromeDriver();
			logger.info("launched Chrome Browser");
			setup();
			
			
			
			
		}else if(browserName.equalsIgnoreCase("fireFox")) {
			
			System.setProperty("webdriver.gecko.driver", path+"\\Drivers\\geckodriver.exe");
			driver= new FirefoxDriver();
			logger.info("launched fireFox Browser");
			setup();
		}
		
		
		
	}
	
	public static void waits() throws InterruptedException {
		Thread.sleep(3000);
	}
	
	
	
	

}
