package com.automation.ui.testCases;

import java.io.IOException;

import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import com.automation.ui.pom.CreditCard_page;
import com.automation.ui.pom.OrderSummary_Page;
import com.automation.ui.pom.PaymentMode_Page;
import com.automation.ui.pom.ShoppingCart_Page;
import com.automation.ui.utilities.BaseTest;
import com.automation.ui.utilities.Config;
import com.automation.ui.utilities.ReadExcelData;
import com.automation.ui.utilities.ScreenShot;

public class TC_03_CheckOutPillowFailFlow extends BaseTest {
	
	
	@DataProvider(name="getUserData")
	public Object[][] getUserData() throws IOException {
		
		Object[][] arr=ReadExcelData.readUserDetails();
		return arr;
	}
	
	@DataProvider(name="getCardData")
	public Object[][] getCardData() throws IOException {
		
		Object[][] arr=ReadExcelData.readInvalidCardDetails();
		return arr;
	}

	@BeforeClass
	public void initiate() throws IOException {
		
		BaseTest.browser(Config.getData("fireFoxBrowser"));
		BaseTest.navigate(Config.getData("Url"));
	
	}
	
	

	@Test(priority=1,dataProvider="getUserData",enabled=true)
	public void ShoppingCartDetails(String name,String email,String phone,String city,String address,String postal) throws InterruptedException, IOException {
	
		ShoppingCart_Page ShoppingCart_Page=PageFactory.initElements(driver, ShoppingCart_Page.class);
		ShoppingCart_Page.click_BuyButton();
		ShoppingCart_Page.enter_name(name);
		ShoppingCart_Page.enter_email(email);
		ShoppingCart_Page.enter_phoneNo(phone);
		ShoppingCart_Page.enter_city(city);
		ShoppingCart_Page.enter_address(address);
		ShoppingCart_Page.enter_postal(postal);
		ScreenShot.getScreenShot(driver,"ShoppingDetails");
		ShoppingCart_Page.click_checkOutButton();
		
		

	}
	
	@Test(priority=2,dependsOnMethods={"ShoppingCartDetails"})
	public void verify_OrderSummary() throws IOException {
		
		OrderSummary_Page OrderSummary_Page=PageFactory.initElements(driver, OrderSummary_Page.class);
		switchToFrame();
		OrderSummary_Page.click_orderDetails_tab();
		String amount= OrderSummary_Page.getText_amount_textBody();
		OrderSummary_Page.click_shoppingDetails_tab();
		String name= OrderSummary_Page.getText_name_textBody();
		String email= OrderSummary_Page.getText_email_textBody();
		String phoneNo= OrderSummary_Page.getText_phoneNo_textBody();
		String address= OrderSummary_Page.getText_address_textbody();
		ScreenShot.getScreenShot(driver,"OrderSummaryDetails");
		OrderSummary_Page.click_continue_Button();
		
		Assert.assertEquals(amount, "20,000","amount mismatch");
		Assert.assertEquals(name, "ankush","name mismatch");
		Assert.assertEquals(email, "ankushsahu90@gmail.com","Email mismatch");
		Assert.assertEquals(phoneNo, "9028055581","phone Number mismatch");
		Assert.assertEquals(address, "Vijay nagar Indore 452010","phone Number mismatch");
		
	}
	

	@Test(priority=3,dependsOnMethods={"verify_OrderSummary"})
	public void Select_PaymentMode() {
		
		PaymentMode_Page paymentMode_Page=PageFactory.initElements(driver, PaymentMode_Page.class);
		String pageName=paymentMode_Page.getText_payment_span();
		Assert.assertEquals(pageName, "Select Payment","Invalid Page");
		paymentMode_Page.click_creditCard_option();
						
	}
	
	
	@Test(priority=4,dataProvider="getCardData",dependsOnMethods={"Select_PaymentMode"})
	public void Payment_ByCreditCard(String cardNumber,String expiry,String cvv,String passsword) throws InterruptedException, IOException {
		
		CreditCard_page creditCard_page=PageFactory.initElements(driver, CreditCard_page.class);
		creditCard_page.enter_creditCardNumber_TextBox(cardNumber);
		creditCard_page.enter_expiryData_TextBox(expiry);
		String msg=creditCard_page.get_InvalidCardDetailsMsg();
		Assert.assertEquals(msg,"Invalid card number","Invalid Card Details entered");
		
						
	}

	
	
	
	@AfterTest
	public void exit() {
		driver.quit();
	}
	
}
