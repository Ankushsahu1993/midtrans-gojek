1) Tool Selection
	1) UI test Framework(Selenium).
	2) Programming language(Java)
	3) Write testcase using(TestNG)
	4) Repository (BitBucket) Ankush@123 ankushsahu1208@gmail.com (ankushsahu1993)
	5) Project Management (Maven)
	6) CI Tool (Jenkins)
	7) Reporting (Extent Report)
	8) IDE (Eclipse)
2) System Requirements
	1) Install Maven 
	2) Install Java
	3) Install Jenkins Server

3) Maven
	1)Create Maven quick start project.
	2) Dependencies
		1) selenium-java
		2) testng
		3) Apache POI
		4) extentreports
		5) commons-io
		6)log4j-api
		7)log4j-core

	3)plugins
		1) maven-surefire-plugin
		2) maven-compiler-plugin
		3) maven-postman-plugin

4) Drivers
		1) ChromeDriver
		2) GeckoDriver(FireFox)	


